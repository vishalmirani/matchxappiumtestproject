#Description: Maatch X App Automation using Appium for Validation & Verification Testing

Automation Tool: Appium + Selenium

Language: Java

Management Tool: Maven

Report Tool: Allure Report

OS Support: Android, iOS

#Project Instruction
1. Clone the repository in your system
2. In Source Folder you will see 2 package. master & userAccount
3. In master package there will be 3 class
	a. configue
	b. register_field_master
	c. variable
4. variable file will have general variable declaration.
5. register_field_master file will have function for each element event
6. configue fill will have all configuration for Android & IOS application
7. Now in userAccount Package there will be 1 class
	a. registeration
8. In registeration file you will able to see 2 Test Case
	a. Validation for Country Field Mandatory
	b. Verification for Submit Process Text
9. There is also pom.xml file in root folder which have maven dependency.
10. To run this test using TestNG, there will be TestSuite\Register folder
11. In this folder, we have registration.xml file in which we will mention same test class
12. To identify iOS or Android, we will use Test Name in this registration.xml file.

Note: In order to execution test we need to run command 'mvn clean test' from root directory. But here we need all software to install in system with Application details.
So,please consider code as framework structure & core logic purpose. 

	