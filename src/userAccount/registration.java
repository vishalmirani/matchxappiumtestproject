package userAccount;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import master.register_field_master;
public class registration extends register_field_master {
 
@Story("User Register Validation")
@Description("User Register validation text verify")
@Severity(SeverityLevel.NORMAL)
@Test (priority = 1, enabled=true)
public void user_register_validation() throws Exception, Throwable {
		
			firstname = "Vishal";
			lastname = "Mirani";		
			country = "Germany";
			email = "Auotmation" + todaysdate + "@mailinator.com";
			password = "P@55w0rd";															
			signup_link();
			firstname_field();
			lastname_field();
			email_address_field();
			password_field();
			register_submit_button();
			try
			{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(obj.getProperty("error_alert_text"))));
				String ErrorText = driver.findElement(By.xpath(obj.getProperty("error_alert_text"))).getText();
				if(ErrorText.contentEquals("Please enter your current residing Country"))
				{
					TestReason = "Pass: Validation Error appear for country field";
					logger.info(TestReason); 
					myscreenshot(driver);
				}	
				else
				{
					TestReason = "Fail: Validation Error not appear for country field";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Validation Error not appear for country field";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("User Register Process")
@Description("User Register process text verify")
@Severity(SeverityLevel.BLOCKER)
@Test (priority = 2, enabled=true)
public void user_register_process() throws Exception, Throwable {
			country_field();
			register_submit_button();
			try
			{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(obj.getProperty("process_text"))));
				String ProcessText = driver.findElement(By.xpath(obj.getProperty("process_text"))).getText();
				if(ProcessText.contentEquals("Data is in processing."))
				{
					TestReason = "Pass: Register process message appear properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}	
				else
				{
					TestReason = "Fail: Register process message not appear properly";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Register process message not appear properly";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	

}
}