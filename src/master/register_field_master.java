package master;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class register_field_master extends configue{
		
public void signup_link() throws Exception 
{
		try
		{
			wait.until(ExpectedConditions.elementToBeClickable(By.id(obj.getProperty("signup_link"))));
			driver.findElement(By.id(obj.getProperty("signup_link"))).click();
			logger.info("Signup link click Successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Signup link not found";
			logger.info(TestReason);
			myscreenshot(driver);
			Assert.fail(TestReason);
		}
}
public void firstname_field() throws Exception
{
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(obj.getProperty("firstname_field"))));
			driver.findElement(By.id(obj.getProperty("firstname_field"))).sendKeys(firstname);
			logger.info("First Name Inserted");
		}
		catch(Exception e)
		{
			TestReason = "First Name field not found";
			logger.info(TestReason);
			myscreenshot(driver);	
			Assert.fail(TestReason);
		}
}

public void lastname_field() throws Exception
{
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(obj.getProperty("lastname_field"))));
			driver.findElement(By.id(obj.getProperty("lastname_field"))).sendKeys(lastname);
			logger.info("Last Name Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Last Name field not found";
			logger.info(TestReason);
			myscreenshot(driver);	
			Assert.fail(TestReason);
		}
}
public void email_address_field() throws Exception
{
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(obj.getProperty("email_field"))));
			driver.findElement(By.id(obj.getProperty("email_field"))).sendKeys(email);
			logger.info("Email Address Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Email Address field not found";
			logger.info(TestReason);
			myscreenshot(driver);	
			Assert.fail(TestReason);
		}
}
public void password_field() throws Exception
{
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(obj.getProperty("password_field"))));
			driver.findElement(By.id(obj.getProperty("password_field"))).sendKeys(password);
			logger.info("Password Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Password field not found";
			logger.info(TestReason);
			myscreenshot(driver);	
			Assert.fail(TestReason);
		}
}
public void country_field() throws Exception
{
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(obj.getProperty("country_field"))));
			driver.findElement(By.id(obj.getProperty("country_field"))).sendKeys(country);
			logger.info("Country Name Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Country Name field not found";
			logger.info(TestReason);
			myscreenshot(driver);	
			Assert.fail(TestReason);
		}
}

public void register_submit_button() throws Exception 
{
		try
		{
			wait.until(ExpectedConditions.elementToBeClickable(By.id(obj.getProperty("register_button"))));
			driver.findElement(By.id(obj.getProperty("register_button"))).click();
			logger.info("Register Button click Successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Register Button not found";
			logger.info(TestReason);
			myscreenshot(driver);
			Assert.fail(TestReason);
		}
}
}