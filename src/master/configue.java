package master;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Allure;
import io.qameta.allure.Description;

public class configue extends variable {
	
//-----------------------------------Driver Setup-----------------------------------------------------	

public void driverset() throws Exception, InterruptedException,UnsupportedEncodingException,URISyntaxException {
							
	System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
		
		if(envname.contains("Android"))
		{	
			objfile = new FileInputStream(System.getProperty("user.dir") + "//android.properties");
		}
		else
		{
			objfile = new FileInputStream(System.getProperty("user.dir") + "//ios.properties");
		}
		obj = new Properties();
		obj.load(objfile);
			
		PropertyConfigurator.configure(System.getProperty("user.dir") + "//Log4j.properties");
		
		logger = Logger.getLogger("XXXX");
		
	    wait = new WebDriverWait(driver, 30);
	  			
}

@Description("Test Setup Congfiguration")
@BeforeTest(alwaysRun = true,description = "Test Setup Congfiguration")
@org.testng.annotations.Parameters(value={"os","os_version", "environment","automationname","device"})
public void localsetup(@Optional String os, @Optional String version, @Optional String environment, String autoname, @Optional String device) throws Exception {
											
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyHHmmSS");
		Date date = new Date();
		todaysdate = formatter.format(date);
							
        DesiredCapabilities capabilities = new DesiredCapabilities();
              
       	if(autoname.contains("Android"))
       	{
	        capabilities.setCapability("os_version", "9.0");
	        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
	        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "pixel3");
	        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS,true);
	        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,AutomationName.ANDROID_UIAUTOMATOR2);
	        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "20000");	
	        capabilities.setCapability("appPackage", "com.XXX.XXX");
	        capabilities.setCapability("bundleId", "com.XXX.XXX");
	        capabilities.setCapability("appActivity", "com.XXXX.activities.SplashActivity");
       	}
       	else
       	{
	        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 11 Pro Max");
	        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"iOS");
       		capabilities.setCapability("os_version", "14.0");
	        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,AutomationName.IOS_XCUI_TEST);
	        capabilities.setCapability("bundleId", "com.fenderDigital.TuneAndTone");
	        capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
	        capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
	        capabilities.setCapability("resetOnSessionStartOnly", true);
	        capabilities.setCapability("autoLaunch", "true");
	        capabilities.setCapability(MobileCapabilityType.APP,System.getProperty("user.dir") + "/XXX.app");
       	}
        
        capabilities.setCapability("autoAcceptAlerts",true);
          	     
       	
       	if(autoname.contains("Android"))
       	{	
       		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub/"), capabilities);
       	}
       	else
       	{
       		driver = new IOSDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub/"), capabilities);
       	}
       	envname = autoname;     	
	    this.driverset();
	    logger.info(envname);
	    
}
@Description("After Test Exit Congfiguration")
@AfterTest(alwaysRun = true,description = "After Test Exit Congfiguration")
public void aftertest() throws Exception
{
    	logger.info("App going to close.");
    	driver.quit();
}
//---------------------------------Taking Screen Shot---------------------------------------------------------	
	

public void myscreenshot(WebDriver driver) throws InterruptedException
{			
		try 
		{
			TakesScreenshot screenShot = (TakesScreenshot)driver;	
			File screenshotFile = screenShot.getScreenshotAs(OutputType.FILE);
			Thread.sleep(2000);
			Allure.addAttachment(TestReason, new FileInputStream(screenshotFile));
		} 
		catch (FileNotFoundException e)
		{
			logger.info("Screenshot not able to taken or attached in Allure Report");
		}	
}
public void scrollDown() {
	    int pressX = driver.manage().window().getSize().width / 2;
	    int bottomY = driver.manage().window().getSize().height * 4/5;
	    int topY = driver.manage().window().getSize().height / 8;
	    scroll(pressX, bottomY, pressX, topY);
}
public void scroll(int fromX, int fromY, int toX, int toY) {
	    @SuppressWarnings("rawtypes")
		TouchAction touchAction = new TouchAction(driver);
	    touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();
}
public void swipeleft() {
	
    	Dimension size = driver.manage().window().getSize();
    	int startX = 0;
        int endX = 0;
        int startY = 0;
        @SuppressWarnings("unused")
		int endY = 0;
		startY = size.height/4 * 3;
		startX = (int) (driver.manage().window().getSize().width * 0.90);
		endX = (int) (driver.manage().window().getSize().width * 0.05);

		swipe(startX, startY, endX, startY);
}
public void swipe(int startX, int startY, int endX, int endY) {
	
    @SuppressWarnings("rawtypes")
	TouchAction touchAction = new TouchAction(driver);
    touchAction.longPress(PointOption.point(startX, startY)).moveTo(PointOption.point(endX, startY)).release().perform();
}
}