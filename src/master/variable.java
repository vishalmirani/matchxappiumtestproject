package master;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class variable {

	@SuppressWarnings("rawtypes")
	protected AppiumDriver driver = null;
    @SuppressWarnings("rawtypes")
	AndroidDriver androidDriver = (AndroidDriver) driver;
    @SuppressWarnings("rawtypes")
	IOSDriver iosDriver = (IOSDriver) driver;

	public HashMap<String, Object> params;
	FileInputStream objfile;
	protected String envname;
	protected String firstname;
	protected String lastname;
	protected String password;
	protected String newpassword;
	protected String country;
	protected String mobile;
	public String email;
	protected String TestResult;
	protected String TestReason;
	protected String todaystestdate;
	public String todaysdate;
	protected String testresult;
	protected String finalresult;
	protected String final_result;
	protected String Test_Module_Name;
	protected Logger logger;
	protected Properties obj;
	protected WebDriverWait wait;	
	protected String screenshotname;	
	public String URLS;
}
